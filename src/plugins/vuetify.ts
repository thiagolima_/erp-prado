/**
 * plugins/vuetify.ts
 *
 * Framework documentation: https://vuetifyjs.com`
 */

// Styles
import '@mdi/font/css/materialdesignicons.css'
import 'vuetify/styles'

import { createVuetify } from 'vuetify'
import { lightTheme } from '@/utils'
import { VStepper } from 'vuetify/labs/VStepper'
import { VDataTable } from 'vuetify/labs/VDataTable'
import { pt } from 'vuetify/locale'

// https://vuetifyjs.com/en/introduction/why-vuetify/#feature-guides
export default createVuetify({
    components: {
        VStepper,
        VDataTable,
    },
    defaults: {
        VBtn: {
            color: 'primary',
            variant: 'flat',
            rounded: 'lg',
            class: 'text-none',
        },
        VTextField: {
            variant: 'outlined',
            density: 'compact',
        },
        VDataTable: {
            itemsPerPageOptions: [
                { value: 5, title: '5' },
                { value: 10, title: '10' },
                { value: 25, title: '25' },
                { value: 50, title: '50' },
                { value: 100, title: '100' },
                { value: -1, title: 'Todos' },
            ],
        },
    },
    theme: {
        defaultTheme: 'lightTheme',
        themes: {
            lightTheme,
            // dark: {
            //     colors: {
            //         primary: lightTheme.colors.primary,
            //     },
            // },
        },
    },
    locale: {
        locale: 'pt',
        messages: { pt },
    },
})
