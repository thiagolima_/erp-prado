import { ref, onMounted } from 'vue'

export function useLocalStorage(key: string) {
    const data = ref()

    const setValue = (value: string | null) => {
        localStorage.setItem(key, JSON.stringify(value))
        data.value = value
    }

    const getValue = () => {
        const storedValue = localStorage.getItem(key)
        if (storedValue) {
            data.value = JSON.parse(storedValue)
        }
    }

    const removeValue = () => {
        localStorage.removeItem(key)
        data.value = null
    }

    onMounted(() => {
        getValue()
    })

    return {
        data,
        setValue,
        removeValue,
    }
}
