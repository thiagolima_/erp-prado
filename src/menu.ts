const menu = [
    {
        name: 'inicio',
        label: 'Inicio',
        path: '/',
        icon: 'mdi-home',
    },
    {
        name: 'rh',
        label: 'Recursos Humanos',
        path: '/',
        icon: 'mdi-briefcase-account',
    },
    {
        name: 'financeiro',
        label: 'Financeiro',
        path: '/',
        icon: 'mdi-currency-usd',
    },
    {
        name: 'usuarios',
        label: 'Usuarios',
        path: '/',
        icon: 'mdi-account',
    },
]

export default menu
